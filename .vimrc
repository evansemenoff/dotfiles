:set termguicolors
:set colorcolumn=80
:colorscheme bat
:set nocp
:syntax on
:set backspace=2
:set tabstop=4
:set expandtab
:set shortmess=I
au BufReadPost *.grm set filetype=sml
:set hlsearch
:set rnu
:set autoindent
:nnoremap <leader>vrc :vsplit $MYVIMRC<cr>
:nnoremap <leader>rlrc :source $MYVIMRC<cr>
