if [ -z "${PS1:-}" ]; then
    return
fi

##### SHOPTS #####

shopt -s histappend
shopt -s checkwinsize
shopt -s cdspell
shopt -s autocd

##### EXPORTS #####
export PS1="\[\e[36m\]\u\[\e[m\]@\[\e[32m\]\h\[\e[m\]:[\[\e[33m\]\w\[\e[m\]] "
export EDITOR='vim'
export PAGER='less'
export CDPATH='.:~:~/Documents'
##### ALIASES #####
alias th="mate-terminal . &"
alias open='xdg-open'
alias more='less'
alias top='htop'
alias grep='egrep --color=auto'
alias gcc='gcc -Wall -Wextra -pedantic'
alias l='ls --color'
alias ls='ls --color'
alias ll='ls -la --color'
alias today="vim $(date +%Y_%B_%d.txt | tr [:upper:] [:lower:])"
alias whost="cat /etc/hosts | grep -i"
alias cpj="vim /home/evan/.curproj"

#### FUNCS ####

### NEEDED FOR NUMBUFF TO WORK PROPERLY ###
jmp(){
    jump $1
    source /home/evan/.nb/.config/jumpfile
}

wim(){
    vim $(which $1)
}

cd(){
    pushd "$@" > /dev/null
}

_pager() {
    READLINE_LINE="less ${READLINE_LINE}" 
}

back() {
    popd > /dev/null
}


bind '"\e[1;3A":"cd ..\n"'
bind '"\e[1;3D":"back \n"'
bind '"\C-t":"top\n"'
bind -x '"\C-p":_pager'

### RC FILE MGMT ####
    
alias rlrc='source ~/.bashrc; source ~/.localrc'
alias brc='vim ~/.bashrc; rlrc'
alias lrc='vim ~/.localrc'
alias vrc='vim ~/.vimrc'

urc(){
    cd ~/dotfiles
    git pull
    cp .vimrc ..
    cp .bashrc ..
    cp .tmux.conf ..
    rlrc
}

source ~/.localrc
